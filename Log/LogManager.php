<?php

namespace Alpha\Bundle\ActionLogBundle\Log;

use Aws\DynamoDb\DynamoDbClient;

class LogManager
{
    protected $client = NULL;
    protected $active = FALSE;

    public function __construct($key, $secret, $region, $active = FALSE) {
        $this->client = DynamoDbClient::factory(array(
            'key'    => $key,
            'secret' => $secret,
            'region' => $region
        ));

        $this->active = $active;
    }

    public function logAction($accountId, $action, array $data = array()) {
        // If not active return as positive
        if (!$this->active) return TRUE;

        $date = new \DateTime();
        $date->setTimezone(new \DateTimeZone('UTC'));

        // Clean current data
        foreach ($data as $key => $value) {
            if ($value == null) {
                unset($data[$key]);
            }

            if ($value instanceof \DateTime) {
                $data[$key] = $value->format(\DateTime::ISO8601);
            }
        }

        $data['accountId'] = $accountId;
        $data['action'] = $action;
        $data['takenAt'] = $date->format(\DateTime::ISO8601);

        $result = $this->client->putItem(array(
            'TableName' => 'account_actions',
            'Item' => $this->client->formatAttributes($data),
            'ReturnConsumedCapacity' => 'TOTAL'
        ));
    }
}